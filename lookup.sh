#!/bin/bash

DATA="srcfiles/data"

if [ "$1" = "german" ]; then
  LANG=$DATA/"de-dc2/dc-german.mmorph"
  ./srcfiles/bin/mmorph3 -b -m $LANG $2 $3
elif [[ "$1" =~ ^(english|french|italian|spanish)$ ]]; then
  LANG=$DATA/$1/$1".mmorph"
  ./srcfiles/bin/mmorph3 -m $LANG $2 $3
else
  echo "Usage: lookup.sh LANG [INFILE [OUTFILE]]"
  exit
fi
