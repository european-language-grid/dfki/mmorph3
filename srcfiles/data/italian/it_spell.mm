;; mmorph program   -   Spelling rules for Italian - 
;; accompany the Italian grammar for morphology, 
;; author S. Manzi, 1996 <Sandra.Manzi@issco.unige.ch>
;;   rev. 1997



@ Classes

Vowel :	a e i o u y � � � � � � h

Consonant: b c d f g h j k l m n p q r s t v w x z

VorCG : a e i o u y � � � � � � h c g
CorG :	c g
EorI :  e i 
MorT: m t 
DorG: d g
M_t_c_v: m t c v
A_e_i : a e i
NorR: n r 
LorN: l n
Verb_re: r e
VowelorNorR : Vowel NorR


@ Pairs

Insert_i:   i / <>
Insert_h:   h / <>
Insert_e:   e / <>

Mtcv: m/m  t/t  c/c v/v

;; hack to match either morpheme (+) or word (~) boundary
Boundary: <>/?	

No_h :   <> / no_h
No_i :   <> / no_i
H_e :  <> / h_e

@ Spelling


;; stops econimico+i = economichi  with application of rule H-insert2
Rem_he: <=> - H_e -

;;  economico+i = economici      economico+e = economiche
H_insert: <=>
   CorG H_e - Insert_h - * e 

;; lungo+i = lung+h+i    lung+issim = lung+h+issim
;; antico+i/e = antichi/e
H_insert2: <=>    
    CorG - Insert_h - * EorI

;;  diping+i = dipingi
Del_H: <=> - No_h -


;; mangi+er� = manger�
Delete_I: <=>
   CorG - <>/i - * e

;; mogli+i = mogl+i    mangi+iamo = mangiamo
Del_I: <=> 
     - <>/i - * i


;; diret-t-or-e -> diret-t-ric-e
;; alleva-t-or-e -> alleva-t-ric-e 
trice1: <=>
  t - <>/o r i/<> c/<> - * N_ASfix[gender=feminine nb=sing|plu n_a_topol=e]


;; spelling rules for clitic-specific changes
;; gli+lo = glielo
more_change_i: <=>
   g l i - Insert_e - * LorN

;;  mi+lo = melo 
change_i : <=>  
     + Mtcv - e/i - * LorN 

;; portare+mi = portarmi
Del_e : <=> 
   Vowel r  - <> / e  - * Clsuff[clD!=empty]
                          Clsuff[clA!=empty]

;; ;; vin-c-ere -> vin + s +i
C_to_s : <=> 
  NorR  - s/c - No_h * 
                 Vsuff[conj=conj2_si finite=yes tense=past mood=ind nb=sing person=1|3 clitic=no]
                 Vsuff[conj=conj2_si finite=yes tense=past mood=ind nb=plu person=3 clitic=no]


;; accen-d-ere acce-s-i   
D_to_s : <=> 
  NorR  - s/d - * 
                 Vsuff[conj=conj2_si finite=yes tense=past mood=ind nb=sing person=1|3 clitic=no]
                 Vsuff[conj=conj2_si finite=yes tense=past mood=ind nb=plu person=3 clitic=no]

;; divi-d-ere -> divi-s-i
Del_D : <=> 
  Vowel  - s/d - * 
                 Vsuff[conj=conj2_si finite=yes tense=past mood=ind nb=sing person=1|3 clitic=no]
                 Vsuff[conj=conj2_si finite=yes tense=past mood=ind nb=plu person=3 clitic=no]

;; dipin-g-ere dipin-s-i
G_to_s : <=> 
  NorR  - s/g - No_h * 
                 Vsuff[conj=conj2_si finite=yes tense=past mood=ind nb=sing person=1|3 clitic=no]
                 Vsuff[conj=conj2_si finite=yes tense=past mood=ind nb=plu person=3 clitic=no]


;; vin-c-ere  vin-t-o/a/e/i     dipin-g-ere dipin-t-o/a/e/i
Del_cu : <=>
  NorR - <>/c No_h * <>/u - t

Del_gu : <=>
  NorR - <>/g No_h * <>/u - t
