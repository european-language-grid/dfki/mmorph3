
	Conj[form=surface type=coord]
"allora" = "allora"
"allorch�"
"anche" = "anche"
"anzi" = "anzi"
"bens�" = "bens�"
"che" = "che"
"cio�" = "cio�"
"dunque" = "dunque"
"e" = "e"
"ebbene" = "ebbene"
"ed" = "ed"
"eppure" = "eppure"
"giacch�" = "giacch�"
"infatti" = "infatti"
"ma" = "ma"
"n�" = "n�"
"neanche" = "neanche"
"nemmeno" = "nemmeno"
"neppure" = "neppure"
"nonch�" = "nonch�"
"nondimeno" = "nondimeno"
"o" = "o"
"o" = "o" 
"od" = "od"
"oppure" = "oppure"
"ossia" = "ossia"
"ovvero" = "ovvero"
"per�" = "per�"
"perci�" = "perci�"
"pertanto" = "pertanto"
"piuttosto" = "piuttosto"
"poich�" = "poich�"
"pure" = "pure"
"quindi" = "quindi"
"se" = "se"
"sebbene" = "sebbene"
"sia" = "sia"
"tuttavia" = "tuttavia"


	Conj[form=surface type=subord]
"affinch�" = "affinch�"
"ancorch�" = "ancorch�"
"bench�" = "bench�"
"che" = "che"
"come" = "come"
"cosicch�"
"finch�"
"fuorch�" = "fuorch�"
"giacch�" = "giacch�"
"laddove" = "laddove"
"mentre" = "mentre"
"perch�" = "perch�"
"poich�" = "poich�"
"purch�" = "purch�"
"qualora" = "qualora"
"quando" = "quando"
"quantunque" = "quantunque"
"se" = "se"
"sebbene" = "sebbene"
"seppure" = "seppure"
"siccome" = "siccome"


	Adv[form=surface]
"abbastanza" = "abbastanza"
"adesso" = "adesso"
"allora" = "allora"
"almeno" = "almeno"
"alquanto" = "alquanto"
"altrettanto" = "altrettanto"
"altrimenti"
"altrove" = "altrove"
"altres�" 
"ancora" = "ancora"
"appena" = "appena"
"appunto" = "appunto"
"assai" = "assai"
"bene" = "bene"
"certo" = "certo"
"come" = "come" 
"cos�"
"dapprima"
"davanti" = "davanti"
"dentro" = "dentro"
"dietro" = "dietro"
"donde"
"dopo" = "dopo"
"dove" = "dove" 
"finora" = "finora"
"forse" = "forse"
"forte" = "forte"
"fuori" = "fuori"
"gi�" = "gi�"
"gi�" = "gi�"
"indietro" = "indietro"
"infatti" = "infatti"
"infine"
"inoltre"
"insomma" = "insomma"
"l�" = "l�"
"l�" = "l�"
"laggi�" = "laggi�"
"lass�" = "lass�"
"lontano" = "lontano"
"mai" = "mai"
"male" = "male"
"mica" = "mica"
"molto" = "molto"
"neanche" = "neanche"
"nemmeno" = "nemmeno"
"neppure" = "neppure"
"niente" = "niente"
"no" = "no"
"non" = "non"
"nulla" = "nulla"
"oltre" = "oltre"
"ora" = "ora"
"ormai" = "ormai"
"parecchio" = "parecchio"
"perfino" = "perfino"
"persino" = "persino"
"piano" = "piano"
"poco" = "poco"
"poi" = "poi"
"presso" = "presso"
"presto" = "presto"
"prima" = "prima"
"proprio" = "proprio"
"qua" = "qua"
"quaggi�" = "quaggi�"
"quando" = "quando"
"quasi" = "quasi"
"quass�" = "quass�"
"qui" = "qui"
"s�" = "s�"
"sempre" = "sempre"
"sopra" = "sopra"
"soprattutto" = "soprattutto"
"sotto" = "sotto"
"sovente" = "sovente"
"spesso" = "spesso"
"su" = "su"
"subito" = "subito"
"talora" = "talora"
"talvolta" = "talvolta"
"tanto" = "tanto"
"tardi" = "tardi"
"troppo" = "troppo"
"vicino" = "vicino"
"volentieri" = "volentieri"

;; 
	Prep[art=none form=surface]
"a"
"ad" = "a"
"accanto"
"accosto"
"addentro"
"addosso"
"anzi"
"appetto"
"appi�"
"appresso"
"assieme"
"attorno"
"attraverso"
"avanti"
"circa"
"con"
"contro"
"da"
"dappresso"
"dattorno"
"davanti"
"dentro"
"di"
"dietro"
"dinanzi"
"dintorno"
"dirimpetto"
"discosto"
"dopo"
"durante"
"eccetto"
"entro"
"fino"
"fra"
"frammezzo"
"fuorch�"
"fuori"
"gi�"
"giusta"
"in"
"incontro"
"infino"
"innanzi"
"insieme"
"intorno"
"invece"
"l�"
"lontano"
"lunghesso"
"lungi"
"lungo"
"malgrado"
"mediante"
"meno"
"merc�"
"oltre"
"per"
"presso"
"sotto"
"stante"
"su"
"tra"
"tramite"
"tranne"
"vece"
"verso"
"vicino"

;; pl_m pl_f sg_m sg_f none

	Prep[art=pl_m form=surface]
"agli" = "a"
"ai" = "a"
"sugli" = "su"
"sui" = "su"
"negli" = "in"
"nei" = "in"
"dagli" = "da"
"dai" = "da"
"cogli" = "con"
"coi" = "con"
"degli" = "di"
"dei" = "di"
"pei" = "per"

	Prep[art=sg_m form=surface]
"al" = "a"
"allo" = "a"
"all'" = "a"
"sul" = "su"
"sullo" = "su"
"sull'" = "su"
"nel" = "in"
"nello" = "in"
"nell'" = "in"
"dallo" = "da"
"dall'" = "da"
"dal" = "da"
"col" = "con"
"collo" = "con"
"del" = "di"
"dello" = "di"
"pel" = "per"

	Prep[art=sg_f form=surface]
"alla" = "a"
"all'" = "a"
"sulla" = "su"
"sull'" = "su"
"nella" = "in"
"nell'" = "in"
"dalla" = "da"
"dall'" = "da"
"colla" = "con"
"della" = "di"
"dell'" = "di"

	Prep[art=pl_f form=surface]
"alle" = "a"
"sulle" = "su"
"nelle" = "in"
"dalle" = "da"
"colle" = "con"
"delle" = "di"


	Art[gender=masculine nb=sing form=surface]
"lo"
"il"
"l'"

	Art[gender=feminine nb=sing form=surface]
"la"
"l'"

	Art[gender=masculine nb=plu form=surface]
"i"
"gli"

	Art[gender=feminine nb=plu form=surface]
"le"

	Indef[nb=sing gender=masculine form=surface]
"alcun" = "alcuno" 
	Indef[nb=sing gender=masculine form=surface]
"alcuno" = "alcuno" 
	Indef[nb=sing gender=feminine form=surface]
"alcuna" = "alcuno" 
	Indef[nb=sing gender=feminine form=surface]
"alcun'" = "alcuno" 
	Indef[nb=plu gender=masculine form=surface]
"alcuni" = "alcuno"
	Indef[nb=plu gender=feminine form=surface]
"alcune" = "alcuno"

	Indef[nb=sing gender=masculine form=surface]
"ciascuno" = "ciascuno" 
	Pronom[gender=masculine nb=plu form=surface]
"ciascun" = "ciascuno"
	Indef[nb=sing gender=feminine form=surface]
"ciascuna" = "ciascuno" 
	Indef[nb=sing gender=masculine form=surface]
"diverso" = "diverso" 
	Indef[nb=plu gender=masculine form=surface]
"diversi" = "diverso" 
	Indef[nb=sing gender=feminine form=surface]
"diversa" = "diverso" 
	Indef[nb=plu gender=feminine form=surface]
"diverse" = "diverso" 

	Indef[nb=sing gender=masculine form=surface]
"altro" = "altro" 
	Indef[nb=plu gender=masculine form=surface]
"altri" = "altro" 
	Indef[nb=sing gender=feminine form=surface]
"altra" = "altro" 
	Indef[nb=plu gender=feminine form=surface]
"altre" = "altro" 

	Indef[nb=sing gender=masculine form=surface]
"taluno" = "taluno" 
	Indef[nb=plu gender=masculine form=surface]
"taluni" = "taluno" 
	Indef[nb=sing gender=feminine form=surface]
"taluna" = "taluno" 
	Indef[nb=plu gender=feminine form=surface]
"talune" = "taluno" 

	Indef[nb=sing gender=masculine form=surface]
"certo" = "certo" 
	Indef[nb=plu gender=masculine form=surface]
"certi" = "certo" 
	Indef[nb=sing gender=feminine form=surface]
"certa" = "certo" 
	Indef[nb=plu gender=feminine form=surface]
"certe" = "certo" 

	Indef[nb=sing gender=masculine form=surface]
"certuno" = "certuno" 
	Indef[nb=plu gender=masculine form=surface]
"certuni" = "certuno" 
	Indef[nb=sing gender=feminine form=surface]
"certuna" = "certuno" 
	Indef[nb=plu gender=feminine form=surface]
"certune" = "certuno" 

	Indef[nb=sing form=surface]
"chiunque"

	Indef[nb=sing gender=masculine form=surface]
"nessuno" = "nessuno" 
	Indef[nb=sing gender=feminine form=surface]
"nessuna" = "nessuno" 

	Indef[nb=sing gender=masculine form=surface]
"niente" = "niente" 
	Indef[nb=sing gender=masculine|feminine form=surface]
"ogni" = "ogni" 
	

	Indef[nb=sing gender=masculine form=surface]
"parecchio" = "parecchio" 
	Indef[nb=plu gender=masculine form=surface]
"parecchi" = "parecchio" 
	Indef[nb=sing gender=feminine form=surface]
"parecchia" = "parecchio" 
	Indef[nb=plu gender=feminine form=surface]
"parecchie" = "parecchio" 

	Indef[nb=sing gender=masculine|feminine form=surface]
"qualche" = "qualche" 

	Indef[nb=sing gender=masculine form=surface]
"qualsiasi" = "qualsiasi" 
	Indef[nb=sing gender=masculine form=surface] 
"qualsivoglia" = "qualsivoglia" 
	Indef[nb=sing gender=masculine form=surface]
"qualunque" = "qualunque" 

	Pronom[nb=sing person=1 type_s=perso form=surface case=nom]
"io" = "io" 
	Pronom[nb=sing person=2 type_s=perso form=surface case=nom]
"tu" = "tu" 
	Pronom[nb=sing person=3 type_s=perso form=surface case=nom]
"egli" = "egli" 
	Pronom[nb=sing person=3 type_s=perso form=surface case=nom]
"esso" = "egli" 
	Pronom[nb=sing person=3 type_s=perso form=surface case=nom]
"lui" = "egli" 
	Pronom[gender=feminine nb=sing person=3 type_s=perso form=surface case=nom]
"ella" = "egli" 
	Pronom[gender=feminine nb=sing person=3 type_s=perso form=surface case=nom]
"lei" = "egli" 
	Pronom[gender=feminine nb=sing person=3 type_s=perso form=surface case=nom]
"essa" = "egli" 
	Pronom[nb=plu person=1 type_s=perso form=surface case=nom]
"noi" = "noi" 
	Pronom[nb=plu person=2 type_s=perso form=surface case=nom]
"voi" = "voi" 
	Pronom[nb=plu person=3 type_s=perso form=surface case=nom]
"loro" = "loro" 
	Pronom[nb=plu person=3 type_s=perso form=surface case=nom]
"essi" = "essi" 
	Pronom[gender=feminine nb=plu person=3 type_s=perso form=surface case=nom]
"esse" = "esse" 
	Pronom[nb=sing person=1 type_s=perso form=surface case=acc]
"me" = "io" 
	Pronom[nb=sing person=2 type_s=perso form=surface case=acc]
"te" = "tu" 
	Pronom[nb=sing person=3 type_s=perso form=surface case=acc]
"lui" = "egli" 
	Pronom[nb=sing person=3 type_s=perso form=surface case=acc]
"esso" = "egli" 
	Pronom[gender=feminine nb=sing person=3 type_s=perso form=surface case=acc]
"lei" = "egli" 
	Pronom[gender=feminine nb=sing person=3 type_s=perso form=surface case=acc]
"essa" = "egli" 
	Pronom[nb=plu person=1 type_s=perso form=surface case=acc]
"noi" = "noi" 
	Pronom[nb=plu person=2 type_s=perso form=surface case=acc]
"voi" = "voi" 
	Pronom[nb=plu person=3 type_s=perso form=surface case=acc]
"loro" = "egli" 
	Pronom[nb=plu person=3 type_s=perso form=surface case=acc]
"essi" = "egli" 
	Pronom[gender=feminine nb=plu person=3 type_s=perso form=surface case=acc]
"esse" = "egli" 
	Pronom[nb=sing person=1 type_s=perso form=surface case=acc]
"mi" = "io" 
	Pronom[nb=sing person=2 type_s=perso form=surface case=acc]
"ti" = "tu" 
	Pronom[nb=sing person=1 type_s=perso form=surface case=acc]
"m'" = "io" 
	Pronom[nb=sing person=2 type_s=perso form=surface case=acc]
"t'" = "tu" 
	Pronom[gender=masculine nb=sing person=3 type_s=perso form=surface case=acc]
"lo" = "egli" 
	Pronom[gender=feminine nb=sing person=3 type_s=perso form=surface case=acc]
"la" = "egli" 
	Pronom[nb=sing person=3 type_s=perso form=surface case=acc]
"l'" = "egli" 
	Pronom[nb=sing person=3 type_s=perso form=surface case=acc]
"essa" = "egli" 
	Pronom[nb=plu person=1 type_s=perso form=surface case=acc]
"ci" = "noi" 
	Pronom[nb=plu person=2 type_s=perso form=surface case=acc]
"vi" = "voi" 
	Pronom[nb=plu person=3 type_s=perso form=surface case=acc]
"li" = "egli" 
	Pronom[nb=plu person=3 type_s=perso form=surface case=acc]
"le" = "egli" 
	Pronom[nb=plu person=3 type_s=perso form=surface case=acc]
"esse" = "egli" 

	Pronom[gender=masculine nb=plu person=3 type_s=perso form=surface]
"entrambi"
	Pronom[gender=feminine nb=plu person=3 type_s=perso form=surface]
"entrambe" = "entrambi"
	Pronom[nb=plu person=3 type_s=perso form=surface]
"ambedue" 
	Pronom[nb=sing person=3 type_s=dimstr form=surface]
"ci�"
	Pronom[gender=masculine nb=plu person=3 type_s=indef form=surface]
"ciascheduno" = "ciascheduno"
	Pronom[gender=feminine nb=plu person=3 type_s=indef form=surface]
"ciascheduna" = "ciascheduno"

;; 	Pronom[gender=masculine nb=plu person=3 type_s=indef form=surface]
;; "ciascuno" = "ciascuno"
;; 	Pronom[gender=masculine nb=plu person=3 type_s=indef form=surface]
;; "ciascun" = "ciascuno"
;; 	Pronom[gender=feminine nb=plu person=3 type_s=indef form=surface]
;; "ciascuna" = "ciascuno"
