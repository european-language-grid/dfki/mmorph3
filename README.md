# MMorph3

MMorph3 is an  update release of **mmorph**, the morphological analyzer developed 
by ISSCO at the University of Geneva in the MULTEXT project (1995). The tool 
can be used in a pipeline architecture to annotate text with a morphosyntactic 
description containing free-definable attributes and values.

## Usage:

* Look-up single word:

  `docker run -it CONTAINER LANG`

  The interactive mode will start (`>`).  
  Enter a single word to display its morphological annotation(s).

* Look-up word list:

  **expected input**: one word per line    
  **output**: morphological annotation(s) of the found words.

  Please note: test file(s) are not included in the dockerfile by default. 
Please make sure that the docker container can access the test file(s) located on your host, e.g.:

  `docker run --mount type=bind,source="$(pwd)"/TESTDIR,target=/root/TEST -it mmorph3 LANG [/root/TEST/INFILE [OUTFILE]]`

  If you specify an output file, the found annotations will be saved to this file. 
Otherwise, they will be displayed in the container.

