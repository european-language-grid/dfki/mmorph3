# set up 32bit architecture
FROM i386/ubuntu:14.04

# set up mmorph3
ADD srcfiles/ ./srcfiles
ADD srcfiles/lib/* ./lib/

# look-up
COPY lookup.sh /
ENTRYPOINT ["/lookup.sh"]
CMD []
EXPOSE 8080

